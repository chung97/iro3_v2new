
#ifndef _DATAPROCESS_H_
#define _DATAPROCESS_H_

#include "stm32f3xx_hal.h"

typedef struct
{
	int16_t VALUE[25];
	uint8_t LVT[25];
	int16_t CNT;
} rxType_t;


void DATAPROCESS_tachGiatri(rxType_t *rx);
#endif
