/*-------------------------------*/
#ifndef _TESTIO_H
#define _TESTIO_H
#include "stm32f3xx_hal.h"

#define IO_TEST_EN					HAL_UART_Transmit(&huart3, (uint8_t*)"[IO_TEST_EN,1]", 14, 100);
#define IO_TEST_DIS					HAL_UART_Transmit(&huart3, (uint8_t*)"[IO_TEST_EN,0]", 14, 100);

#define IN_CHECK_AT_0				HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,1:0]", 14, 100);
#define IN_CHECK_AT_1				HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,1:1]", 14, 100);

#define IN_CHECK_AC_0				HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,2:0]", 14, 100);
#define IN_CHECK_AC_1				HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,2:1]", 14, 100);

#define IN_CHECK_DO1_0			HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,3:0]", 14, 100);
#define IN_CHECK_DO1_1			HAL_UART_Transmit(&huart3, (uint8_t*)"[IN_CHECK,3:1]", 14, 100);


#define OUT_CHECK_VAN_ON		HAL_UART_Transmit(&huart3, (uint8_t*)"[OUT_SET,1:1]", 13, 100);
#define OUT_CHECK_VAN_OFF		HAL_UART_Transmit(&huart3, (uint8_t*)"[OUT_SET,1:0]", 13, 100);

#define OUT_CHECK_BOM_ON		HAL_UART_Transmit(&huart3, (uint8_t*)"[OUT_SET,2:1]", 13, 100);
#define OUT_CHECK_BOM_OFF		HAL_UART_Transmit(&huart3, (uint8_t*)"[OUT_SET,2:0]", 13, 100);

#define AT(status)			 	  HAL_GPIO_WritePin(VTA_GPIO_Port, VTA_Pin, status?GPIO_PIN_SET:GPIO_PIN_RESET)
#define AC(status)				 	HAL_GPIO_WritePin(VCA_GPIO_Port, VCA_Pin, status?GPIO_PIN_SET:GPIO_PIN_RESET)
#define READ_VAN						HAL_GPIO_ReadPin(DKV_GPIO_Port, DKV_Pin)
#define READ_BOM				 		HAL_GPIO_ReadPin(DKB_GPIO_Port, DKB_Pin)
#define CS_R1(status)				HAL_GPIO_WritePin(CS_R1_GPIO_Port, CS_R1_Pin, status?GPIO_PIN_SET:GPIO_PIN_RESET)
#define CS_R2(status)			  HAL_GPIO_WritePin(CS_R2_GPIO_Port, CS_R2_Pin, status?GPIO_PIN_SET:GPIO_PIN_RESET)

void TESTIO_InCheck(void);
void TESTIO_OutCheck(void);

#endif
