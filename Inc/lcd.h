/* lcd.h */
#ifndef _LCD_H
#define _LCD_H
#include "stm32f3xx_hal.h"

void LCD_Init(void);
void LCD_Putc(uint8_t X, uint8_t Y, char c);
void LCD_Puts(uint8_t X, uint8_t Y, char *str);
void LCD_GotoXY(uint8_t X, uint8_t Y);
void LCD_PutInterger(uint8_t X, uint8_t Y, int16_t x);
void LCD_ClearScreen(void);

#endif 
